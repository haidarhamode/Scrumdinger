//
//  EditScrumSheet.swift
//  Scrumdinger
//
//  Created by Haidar on 8/13/23.
//

import SwiftUI

struct EditScrumSheet: View {
    @Binding var scrum: DailyScrum
    @Binding var editingScrum: DailyScrum
    @Binding var isPresentingEditView: Bool
    
    var body: some View {
        if #available(iOS 16.0, *) {
            NavigationStack {
                DetailEditView(scrum: $editingScrum).toolbar {
                    ToolbarItem(placement: .cancellationAction) {
                        Button("Dismiss") {
                            isPresentingEditView = false
                        }
                    }
                    ToolbarItem(placement: .confirmationAction) {
                        Button("Edit") {
                            isPresentingEditView = false
                            scrum = editingScrum
                        }
                    }
                }
            }
        } else {
            NavigationView {
                DetailEditView(scrum: $editingScrum).toolbar {
                    ToolbarItem(placement: .cancellationAction) {
                        Button("Dismiss") {
                            isPresentingEditView = false
                        }
                    }
                    ToolbarItem(placement: .confirmationAction) {
                        Button("Edit") {
                            isPresentingEditView = false
                            scrum = editingScrum
                        }
                    }
                }
            }
        }
    }
}

struct EditScrumSheet_Previews: PreviewProvider {
    static var previews: some View {
        EditScrumSheet(scrum: .constant(DailyScrum.emptyScrum), editingScrum: .constant(DailyScrum.emptyScrum), isPresentingEditView: .constant(true))
    }
}
